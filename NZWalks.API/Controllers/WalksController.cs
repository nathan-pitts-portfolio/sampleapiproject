﻿using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using NZWalks.API.CustomActionFilters;
using NZWalks.API.Models.Domain;
using NZWalks.API.Models.DTO;
using NZWalks.API.Repositories;
using System.Net;

namespace NZWalks.API.Controllers
{
    // /api/Walks
    [Route("api/[controller]")]
    [ApiController]
    public class WalksController : ControllerBase
    {
        private readonly IWalkRepository walkRepository;
        private readonly IMapper mapper;
        private readonly ILogger logger;

        public WalksController(IWalkRepository walkRepository, IMapper mapper, ILogger logger)
        {
            this.walkRepository = walkRepository;
            this.mapper = mapper;
            this.logger = logger;
        }

        //CREATE Walk
        //Post: /api/walks
        [HttpPost]
        [ValidateModel]
        public async Task<IActionResult> Create([FromBody] AddWalkRequestDto addWalkRequestDto)
        {
            // Map Dto to Domain Model
            Walk? walkDomainModel = mapper.Map<Walk>(addWalkRequestDto);

            //Create object
            await walkRepository.CreateAsync(walkDomainModel);

            //Map Domain Model to DTO
            return Ok(mapper.Map<WalkDto>(walkDomainModel));
        }

        //GET All Walks
        //GET /api/walks?filterOn=Name&filterQuery=Track&SortBy=Name&isAscending=true&pageNumber=1&pageSize=10
        [HttpGet]
        public async Task<IActionResult> GetAll([FromQuery] string? filterOn, [FromQuery] string? filterQuery, 
            [FromQuery] string? sortBy, [FromQuery] bool? isAscending, 
            [FromQuery] int pageNumber = 1, [FromQuery] int pageSize = 1000)
        {

            //Create list of Walks from Domain Model
            List<Walk> walkDomainModelList = await walkRepository.GetAllAsync(filterOn, filterQuery, sortBy, isAscending ?? true,
                pageNumber, pageSize);

            //Map DomainModel Walks to DTO
            List<WalkDto> walkDtoList = mapper.Map<List<WalkDto>>(walkDomainModelList);

            return Ok(walkDtoList);

        }

        //GET Walk by ID
        //GET /api/walks/id
        [HttpGet]
        [Route("{id:Guid}")]
        public async Task<IActionResult> GetById([FromRoute]Guid id)
        {
            //Find Walk by Id
            Walk? walkDomainModel = await walkRepository.GetByIdAsync(id);

            if (walkDomainModel == null)
            {
                return NotFound();
            }

            //Convert Domain Model to Dto
            WalkDto walkDto = mapper.Map<WalkDto>(walkDomainModel);

            return Ok(walkDto);
        }

        //UPDATE Walk
        //PUT /api/walks/id
        [HttpPut]
        [Route("{id:Guid}")]
        [ValidateModel]
        public async Task<IActionResult> Update([FromRoute]Guid id, [FromBody]UpdateWalkRequestDto updateWalkRequestDto)
        {
            //Convert Dto to Domain Model
            Walk? walkDomainModel = mapper.Map<Walk>(updateWalkRequestDto);
            
            //Update
            walkDomainModel = await walkRepository.UpdateAsync(id, walkDomainModel);
            
            if (walkDomainModel == null)
            {
                return NotFound();
            }
            
            //Convert Domain Model to DTO
            WalkDto walkDto = mapper.Map<WalkDto>(walkDomainModel);
            
            return Ok(walkDto);
        }

        //DELETE Walk
        //DELETE /api/walks/id
        [HttpDelete]
        [Route("{id:Guid}")]
        public async Task<IActionResult> Delete([FromRoute] Guid id)
        {
            //Delte Walk
            Walk? walkDomainModel = await walkRepository.DeleteAsync(id);

            if (walkDomainModel == null)
            {
                return NotFound();
            }

            //Convert Domain Model to DTO
            WalkDto walkDto = mapper.Map<WalkDto>(walkDomainModel);
            
            return Ok(walkDto);
        }
    }
}
