﻿using AutoMapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using NZWalks.API.CustomActionFilters;
using NZWalks.API.Data;
using NZWalks.API.Models.Domain;
using NZWalks.API.Models.DTO;
using NZWalks.API.Repositories;
using System.Text.Json;

namespace NZWalks.API.Controllers
{
    // https://localhost:7007/api/regions
    [Route("api/[controller]")]
    [ApiController]
    public class RegionsController : ControllerBase
    {
        private readonly IRegionRepository regionRepository;
        private readonly IMapper mapper;
        private readonly ILogger<RegionsController> logger;
        public RegionsController(IRegionRepository regionRepository, IMapper mapper, ILogger<RegionsController> logger)
        {
            this.regionRepository = regionRepository;
            this.mapper = mapper;
            this.logger = logger;
        }

        //GET All Regions
        //GET https://localhost:7007/api/regions/api/regions
        [HttpGet]
        //[Authorize(Roles ="Reader, Writer")]
        public async Task<IActionResult> GetAll()
        {
            //Get Data From Database - Domain Models
            List<Region> regionsDomain = await regionRepository.GetAllAsync();

            //MapExtensions Domain Models to DTOs
            //Return DTOs
            return Ok(mapper.Map<List<RegionDto>>(regionsDomain));
        }

        //GET Region by ID
        //GET https://localhost:7007/api/regions/api/regions
        [HttpGet]
        [Route("{id:Guid}")]
        //[Authorize(Roles = "Reader, Writer")]
        public async  Task<IActionResult> GetById([FromRoute] Guid id)
        {
            //Get Region Domain Model From Database
            Region? regionDomain = await regionRepository.GetByIdAsync(id);

            if (regionDomain == null)
            {
                return NotFound();
            }

            //Map Domain Model to DTO
            //Return Region DTO
            return Ok(mapper.Map<RegionDto>(regionDomain));
        }

        //POST to create new Region
        //GET https://localhost:7007/api/regions/api/regions
        [HttpPost]
        [ValidateModel]
        //[Authorize(Roles = "Writer")]
        public async Task<IActionResult> Create([FromBody] AddRegionRequestDto addRegionRequestDto)
        {
            //Map or Convert DTO to Domain Model
            Region regionDomainModel = mapper.Map<Region>(addRegionRequestDto);

            //Use Domain Model to create Region
            await regionRepository.CreateAsync(regionDomainModel);

            //Map Domain Model back to DTO
            RegionDto regionDto = mapper.Map<RegionDto>(regionDomainModel);

            return CreatedAtAction(nameof(GetById), new { id = regionDto.Id }, regionDto);
            
        }

        //Update Region
        //PUT: https://localhost:7007/api/regions/api/regions/id
        [HttpPut]
        [Route("{id:Guid}")]
        [ValidateModel]
        //[Authorize(Roles = "Writer")]
        public async Task<IActionResult> Update ([FromRoute] Guid id, [FromBody] UpdateRegionRequestDto region)
        {
            //Map DTO to Domain Model
            Region? domainModel = mapper.Map<Region>(region);

            //Check if Region Exists
            domainModel = await regionRepository.UpdateAsync(id, domainModel);

            if (domainModel == null)
            {
                return NotFound();
            }

            //Convert Domain Model to Region Dto
            RegionDto regionDto = mapper.Map<RegionDto>(domainModel);

            return Ok(regionDto);
            
        }

        //Delete Region
        //Delete: https://localhost:7007/api/regions/api/regions/id
        [HttpDelete]
        [Route("{id:Guid}")]
        //[Authorize(Roles = "Writer")]
        public async Task <IActionResult> Delete([FromRoute] Guid id)
        {
            //Delete Region and Check if it Exists
            var regionDomainModel = await regionRepository.DeleteAsync(id);

            if (regionDomainModel == null)
            {
                return NotFound();
            }
            

            //Return deleted Region back
            //Map Domain Model to Dto
            RegionDto regionDto = mapper.Map<RegionDto>(regionDomainModel);


            return Ok(regionDto);
        }
    }
}
